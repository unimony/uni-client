# UniMony Client installation :


### - Clone this repository
```
git clone https://gitlab.com/unimony/uni-client.git
```

### - Install application
```
cd uni-client
npm install
```

### - Run Client in terminal
```
npm run serve
```

### - Open the application

http://localhost:8080


---

## [Learn more with the WIKI](https://gitlab.com/unimony/uni-client/wikis/Home)