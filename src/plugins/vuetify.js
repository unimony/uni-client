import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import colors from 'vuetify/lib/util/colors';

Vue.use(Vuetify);
console.log(colors)
export default new Vuetify({
    theme: {
        dark: true,
        themes: {
            light: {
                primary: colors.cyan.darken2,
                secondary: colors.teal.lighten1,
                accent: colors.shades.black,
                error: colors.red.accent3,
            },
            dark: {
                
                primary: colors.cyan.darken2,
                secondary: colors.teal.lighten1,
                accent: colors.shades.black,
                error: colors.red.accent3,
            },
        },
    },
    
});
