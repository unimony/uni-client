import Vue from 'vue'
import VueRouter from 'vue-router'
//import Home from '../views/home.vue'
//import Login from '../viewslogin.vue'
//import Register from '../views/register.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/home.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/login.vue')
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../views/register.vue')
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/about.vue')
  },
  {
    path: '/profile',
    name: 'profile',
    component: () => import('../views/profile.vue')
  },
  {
    path: '/createWallet',
    name: 'createWallet',
    component: () => import('../views/createWallet.vue')
  },
  {
    path: '/sendPayement/:fromWalletUid',
    name: 'sendPayement',
    component: () => import('../views/sendPayement.vue')
  },
  {
    path: '/receivePayement/:toWalletUid',
    name: 'sendPayement',
    component: () => import('../views/receivePayement.vue')
  },
  {
    path: '/admin',
    name: 'admin',
    component: () => import('../views/admin/dashboard.vue')
  },
  {
    path: '/admin/network',
    name: 'admin',
    component: () => import('../views/admin/node-network.vue')
  },
  {
    path: '/init',
    name: 'init',
    component: () => import('../views/init.vue')
  },
  {
    path: '/boot-node',
    name: 'boot-node',
    component: () => import('../views/boot-node.vue')
  },
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

import axios from 'axios'
axios.interceptors.request.use(
  (config) => {
    let token = localStorage.getItem('access_token');
    if (token) {
      config.headers['x-auth-token'] = token
    }

    return config;
  }, 

  (error) => {
    return Promise.reject(error);
  }
);

export default router

